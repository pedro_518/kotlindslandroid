object Dependencies {

    object Implementation {
        const val appCompat = "androidx.appcompat:appcompat:${Version.appCompat}"
        const val androidXCoreKtx = "androidx.core:core-ktx:${Version.androidXCore}"
        const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Version.constraintLayout}"
        const val recyclerView = "androidx.recyclerview:recyclerview:${Version.recyclerview}"
        const val kotlinStd = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Version.kotlin}"
        const val coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Version.coroutines}"
        const val retrofit = "com.squareup.retrofit2:retrofit:${Version.retrofit}"
        const val retrofitConverterMoshi = "com.squareup.retrofit2:converter-moshi:${Version.retrofit}"
        const val retrofitMoshiAdapters = "com.squareup.moshi:moshi-adapters:${Version.moshi}"
        const val koinCore = "org.koin:koin-core:${Version.koin}"
        const val koinCoreExt = "org.koin:koin-core-ext:${Version.koin}"
        const val koinAndroid = "org.koin:koin-android:${Version.koin}"
        const val koinAndroidScope = "org.koin:koin-androidx-scope:${Version.koin}"
        const val koinAndroidViewModel = "org.koin:koin-androidx-viewmodel:${Version.koin}"
        const val koinAndroidFragment = "org.koin:koin-androidx-fragment:${Version.koin}"
        const val coil = "io.coil-kt:coil:${Version.coil}"
        const val materialComponents = "com.google.android.material:material:${Version.materialComponents}"
        const val paging = "androidx.paging:paging-runtime:${Version.paging}"
    }

    object TestImplementation {
        const val junit = "junit:junit:${Version.junit}"
        const val mockk = "io.mockk:mockk:${Version.mockk}"
        const val truth = "com.google.truth:truth:${Version.truth}"
        const val koinTest = "org.koin:koin-test:${Version.koin}"
    }

    object AndroidTestImplementation {
        const val jUnitExt = "androidx.test.ext:junit:${Version.junitExt}"
        const val truthExtensions = "com.google.truth:truth:extensions:${Version.truth}"
        const val espresso = "androidx.test.espresso:espresso-core:${Version.espresso}"
    }
}