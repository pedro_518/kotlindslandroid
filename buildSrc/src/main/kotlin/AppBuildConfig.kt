object AppBuildConfig {

    const val applicationId = "org.edrodev.kotlindslapp"

    const val compileSdkVersion = 29
    const val targetSdkVersion = 29
    const val minSdkVersion = 23
    const val versionCode = 1
    const val versionName = "0.0.1"

    const val jvmTarget = "1.8"
}