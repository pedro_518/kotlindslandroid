enum class Modules(val value: String) {

    domainCore(":domain:domain-core"),
    dataCore(":data:data-core")
}

enum class Features(val value: String) {
    pokemonList(":feature:pokemonlist")
}