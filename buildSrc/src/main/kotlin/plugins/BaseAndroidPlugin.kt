package plugins

import com.android.build.gradle.BaseExtension
import org.gradle.api.JavaVersion
import org.gradle.api.Project
import org.gradle.kotlin.dsl.dependencies
import org.gradle.kotlin.dsl.getByType

abstract class BaseAndroidPlugin : BasePlugin() {

    override val kotlinPluginId: String = Plugins.kotlinAndroid

    override fun apply(target: Project) {
        super.apply(target)
        with(target) {
            configurePlugins()
            configureAndroid()
            configureDependencies()
        }
    }

    private fun Project.configurePlugins() {
        plugins.apply {
            apply(Plugins.kotlinAndroidExtensions)
        }
    }

    private fun Project.configureAndroid() = this.extensions.getByType<BaseExtension>().run {
        compileSdkVersion(AppBuildConfig.compileSdkVersion)
        buildToolsVersion("29.0.3")
        defaultConfig {
            minSdkVersion(AppBuildConfig.minSdkVersion)
            targetSdkVersion(AppBuildConfig.targetSdkVersion)
            versionCode = AppBuildConfig.versionCode
            versionName = AppBuildConfig.versionName
            testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
        }

        buildTypes {
            getByName("release") {
                isMinifyEnabled = false
                proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
            }

            getByName("debug") {
                isTestCoverageEnabled = true
            }
        }

        compileOptions {
            sourceCompatibility = JavaVersion.VERSION_1_8
            targetCompatibility = JavaVersion.VERSION_1_8
        }

        viewBinding.isEnabled = true
    }



    private fun Project.configureDependencies() {
        dependencies {
            Dependencies.Implementation.apply {
                "implementation"(appCompat)
                "implementation"(androidXCoreKtx)
                "implementation"(constraintLayout)
                "implementation"(recyclerView)
                "implementation"(retrofit)
                "implementation"(koinAndroid)
                "implementation"(koinAndroidScope)
                "implementation"(koinAndroidViewModel)
                "implementation"(koinAndroidFragment)
                "implementation"(coil)
                "implementation"(materialComponents)
            }

            Dependencies.AndroidTestImplementation.apply {
                "androidTestImplementation"(jUnitExt)
                //"androidTestImplementation"(truthExtensions)
                "androidTestImplementation"(espresso)
            }
        }
    }
}