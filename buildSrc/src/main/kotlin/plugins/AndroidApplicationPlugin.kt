package plugins

import com.android.build.gradle.BaseExtension
import org.gradle.api.Project
import org.gradle.kotlin.dsl.getByType

class AndroidApplicationPlugin : BaseAndroidPlugin() {
    override val mainPluginId: String = Plugins.androidApplication

    override fun apply(target: Project) {
        super.apply(target)
        with(target) {
            configureAndroid()
        }
    }

    private fun Project.configureAndroid() = this.extensions.getByType<BaseExtension>().run {
        buildToolsVersion(Version.buildTools)
        defaultConfig {
            applicationId = AppBuildConfig.applicationId
        }
    }
}