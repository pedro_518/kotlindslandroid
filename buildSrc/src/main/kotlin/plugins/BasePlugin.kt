package plugins

import AppBuildConfig
import Dependencies
import Plugins
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.dependencies
import org.gradle.kotlin.dsl.repositories
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

abstract class BasePlugin : Plugin<Project> {

    abstract val mainPluginId: String
    abstract val kotlinPluginId: String

    override fun apply(target: Project) {
        with(target) {
            configurePlugins()
            configureRepositories()
            configureDependencies()
            tasks.withType(KotlinCompile::class.java) {
                kotlinOptions.jvmTarget = AppBuildConfig.jvmTarget
            }
        }
    }

    private fun Project.configurePlugins() {
        plugins.apply {
            apply(mainPluginId)
            apply(kotlinPluginId)
            apply(Plugins.kotlinKapt)
        }
    }

    private fun Project.configureRepositories() {
        repositories {
            jcenter()
        }
    }

    private fun Project.configureDependencies() {
        dependencies {
            Dependencies.Implementation.apply {
                "implementation"(kotlinStd)
                "implementation"(coroutines)
                "implementation"(koinCore)
                "implementation"(koinCoreExt)
            }

            Dependencies.TestImplementation.apply {
                "testImplementation"(junit)
                "testImplementation"(mockk)
                "testImplementation"(truth)
                "testImplementation"(koinTest)
            }
        }
    }

}