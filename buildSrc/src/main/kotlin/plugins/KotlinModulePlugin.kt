package plugins
import Plugins

class KotlinModulePlugin : BasePlugin() {

    override val mainPluginId: String = Plugins.javaLibrary
    override val kotlinPluginId: String = Plugins.kotlin
}