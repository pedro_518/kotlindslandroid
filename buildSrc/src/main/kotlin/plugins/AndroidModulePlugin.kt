package plugins
import com.android.build.gradle.BaseExtension
import org.gradle.api.JavaVersion
import org.gradle.api.Project
import org.gradle.kotlin.dsl.getByType

class AndroidModulePlugin : BaseAndroidPlugin() {
    override val mainPluginId: String = Plugins.androidLibrary

    override fun apply(target: Project) {
        super.apply(target)
        with(target) {
            configurePlugins()
            configureAndroid()
        }
    }

    private fun Project.configurePlugins() {
        plugins.apply {
            apply(Plugins.kotlinAndroid)
            apply(Plugins.kotlinAndroidExtensions)
        }
    }

    private fun Project.configureAndroid() = this.extensions.getByType<BaseExtension>().run {
        compileSdkVersion(AppBuildConfig.compileSdkVersion)
        defaultConfig {
            minSdkVersion(AppBuildConfig.minSdkVersion)
            targetSdkVersion(AppBuildConfig.targetSdkVersion)
            versionCode = AppBuildConfig.versionCode
            versionName = AppBuildConfig.versionName
            testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
        }

        buildTypes {
            getByName("release") {
                isMinifyEnabled = false
                proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
            }

            getByName("debug") {
                isTestCoverageEnabled = true
            }
        }

        compileOptions {
            sourceCompatibility = JavaVersion.VERSION_1_8
            targetCompatibility = JavaVersion.VERSION_1_8
        }
    }
}