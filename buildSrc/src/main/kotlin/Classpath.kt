object Classpath {
    const val androidToolsBuildGradle = "com.android.tools.build:gradle:3.6.1"
    const val kotlinGradlePugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Version.kotlin}"
    const val koinGradlePugin = "org.koin:koin-gradle-plugin:${Version.koin}"
}