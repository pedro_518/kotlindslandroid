plugins {
    `kotlin-dsl`
}

repositories {
    mavenCentral()
    google()
    jcenter()
}

gradlePlugin {
    plugins {
        register("AndroidModulePlugin") {
            id = "androidModulePlugin"
            implementationClass = "plugins.AndroidModulePlugin"
        }

        register("AndroidApplicationPlugin") {
            id = "androidApplicationPlugin"
            implementationClass = "plugins.AndroidApplicationPlugin"
        }

        register("KotlinModulePlugin") {
            id = "kotlinModulePlugin"
            implementationClass = "plugins.KotlinModulePlugin"
        }
    }
}

dependencies {
    /* Example Dependency */
    /* Depend on the android gradle plugin, since we want to access it in our plugin */
    implementation("com.android.tools.build:gradle:3.6.1")

    /* Example Dependency */
    /* Depend on the kotlin plugin, since we want to access it in our plugin */
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:1.3.70")

    /* Depend on the default Gradle API's since we want to build a custom plugin */
    implementation(gradleApi())
    implementation(localGroovy())
}