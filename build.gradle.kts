// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
    repositories {
        google()
        jcenter()
        
    }
    dependencies {
        classpath(Classpath.androidToolsBuildGradle)
        classpath(Classpath.kotlinGradlePugin)

        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        
    }
}

tasks {
    create<Delete>("clean") {
        delete(allprojects.map { it.buildDir })
    }
}
